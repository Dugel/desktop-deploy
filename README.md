# ubuntu2004-setup

Contient les opérations pour un setup de base d'un poste Ubuntu 20.04 pour CISEL

A exécuter avec son compte utilisateur standard

`wget -O - https://gitlab.com/Dugel/desktop-deploy/-/raw/master/ubuntu2004-setup.sh | sudo bash`